// console.log("Hello World")

// [SECTION] While Loop
/*
	Syntax:
		while(condition) {
			codeblock...
		}
*/

let count = 5

// while the value of count is not equal to 0
while(count !== 0) {
	// first iteration: count == 5
	// second iteration: count == 4
	// third iteration: count == 3
	// fourth iteration: count == 2
	// fourth iteration: count == 1
	console.log("While: " + count)
	count--
}

// [SECTION] Do While Loop
/*
	Syntax:
		do {
			codeblock...
		}while(condition)
*/
let number = Number(prompt("Give me a number:"))

do {
	// The current value of number is printed out.
	console.log("Do While: " + number)

	// Increases the value of the number by 1 after every iteration to stop the loop when it reaches 10
	number += 1

}while(number <= 10)

// [SECTION] For Loop
/*
	Syntax:
		for(initialization; condition; finalExpression/iteration) {
			codeblock...
		}
*/

for(let count = 0; count <= 20; count++) {
	console.log("For Loop: " + count)
}

// create a string named myString
let myString = "alex"

// Character in strings may be counted using the .length property
// Strings are special compared to other data types in that access to functions and other pieces of information
console.log(myString.length)

// Accessing elements of a string
// Individual Characters
console.log("Accessing lements individually: " + myString[0])
console.log("Accessing lements individually: " + myString[1])
console.log("Accessing lements individually: " + myString[2])
console.log("Accessing lements individually: " + myString[3])

// A loop that will print out the individual letters of myString variable
for(let x = 0; x < myString.length; x++) {
	// the current value of myString is printed out using its index
	console.log("For Loop: " + myString[x])
}

// create a string named "myName"
let myName = "ALEX"

for(let i = 0; i < myName.length; i++) {
	// the current value of myString is printed out using its index
	if (myName[i].toLowerCase() == "a" || myName[i].toLowerCase() == "e" || myName[i].toLowerCase() == "i" || myName[i].toLowerCase() == "o" || myName[i].toLowerCase() == "u") {
		console.log(3)
	}
	else {
		console.log(myName[i])
	}
}

// [SECTION] Continue and Break Statements

for(let count = 0; count <= 20; count++) {
	// if remainder is equal to 0
	if(count % 2 === 0 ) {
		continue
	}

	console.log("Continue and Break: " + count)

	// if the current value of count is greater than 10, the loop will stop
	if(count > 10) {
		break
	}
}

let name = "alexandro"

for(let a = 0; a < name.length; a++) {
	// will print current letters based on its index
	console.log(name[a])

	// if the vowel is equal to a, continue to the next iterationof the loop
	if(name[a].toLowerCase() === "a") {
		console.log("Continue to the next iteration")
		continue
	}

	// if the current letter is equal to d, stop the loop
	if(name[a].toLowerCase() === "d") {
		break
	}
}